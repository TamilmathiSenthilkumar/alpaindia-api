require('dotenv').config();
const express = require('express');
const cors = require('cors')
const mongoose = require('mongoose');
const routes = require('./src/routes/routes');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');

const port = process.env.PORT || 3000;

const corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "exposedHeaders": "X-Auth",
    "optionsSuccessStatus": 200
}
global.connections = [];
global.clientId = '';
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "100mb" }));
app.use(cors(corsOptions));
app.use('/assets', express.static(__dirname + '/public'));

mongoose.connect(process.env.MONGODB_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('connected to DB');
    }
});

routes(app);

app.listen(port, function (err) {
    if (err) {
    } else {
    }
});

app.use('/', (req, res, next) => {
    var arr = req.url.split("/");
    var now = new Date().toString();
    var log = `${now} : ${req.method} , ${req.url} , ${JSON.stringify(req.body)}`
    fs.appendFile('server.log', log + '\n', (err) => {
        if (err) {
        }
    });
    next();
});

