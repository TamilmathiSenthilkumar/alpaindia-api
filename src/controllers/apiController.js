var User = require('../models/user');

exports.getUsers = (req, res) => {
    User.find({})
        .sort({
            'createdAt': -1
        })
        .then((user) => {
            res.send({
                errorCode: 0,
                msg: 'Success',
                user
            });
        }).catch((e) => {
            res.status(400).send({
                errorCode: 1,
                msg: 'Error!',
                errorDetails: e
            });
        });
}

exports.signIn = (req, res) => {

    var body = req.body;
    User.findByCredentials(body.email, body.password)
        .then((user) => {
            res.header('X-Auth', user.tokens[0].token).send({
                errorCode: 0,
                msg: 'Success',
                user
            });
        }).catch((e) => {
            res.status(400).send({
                errorCode: 1,
                msg: 'Error!',
                errorDetails: e
            });
        });
}

exports.userSave = (req, res) => {
    body = req.body;
    var user = new User(body);

    User.findOne({
            email: user.email
        })
        .then((usr) => {
            if (usr) {
                res.status(400).send({
                    errorCode: 1,
                    msg: 'Failed',
                    errorDetails: 'Email Already In Use!!!'
                });
            } else {
                user.save().then(() => {
                    return user.generateAuthToken()
                }).then((token) => {
                    res.send({
                        errorCode: 0,
                        msg: 'Success',
                        user
                    });
                }).catch((e) => {
                    res.status(400).send({
                        errorCode: 1,
                        msg: 'Error!',
                        errorDetails: e
                    });
                });
            }
        }).catch((e) => {
            res.status(400).send({
                errorCode: 1,
                msg: 'Error!',
                errorDetails: e
            });
        });
}

exports.activityUserSave = (req, res) => {

    var user = new User();
    user.email = req.body.email;
    user.password = req.body.password;

    User.findOne({
            email: user.email
        })
        .then((usr) => {
            if (usr) {
                res.status(400).send({
                    errorCode: 1,
                    msg: 'Failed',
                    errorDetails: 'Email Already In Use!!!'
                });
            } else {
                user.save().then(() => {
                    return user.generateAuthToken()
                }).then((token) => {
                    var payload = {
                        'collectionType': 'User',
                        'referenceDocument': user._id,
                        'action': 'created',
                        'loggedBy': req.user._id
                    }
                    var activitylog = new ActivityLog(payload);
                    activitylog.save();
                    res.send({
                        errorCode: 0,
                        msg: 'Success',
                        user
                    });
                }).catch((e) => {
                    res.status(400).send({
                        errorCode: 1,
                        msg: 'Error!',
                        errorDetails: e
                    });
                });
            }
        }).catch((e) => {
            res.status(400).send({
                errorCode: 1,
                msg: 'Error!',
                errorDetails: e
            });
        });
}

exports.getActivities = (req, res) => {
    // var ActivityModel = require('mongoose-activitylogs/activity-model');
    ActivityLog.find({})
        .populate({
            path: 'loggedBy',
            select: 'fullName email',
            connection: User
        })
        .populate({
            path: 'referenceDocument',
            select: 'email',
            // connection: User
        })
        .then((activities) => {
            res.send({
                errorCode: 0,
                msg: 'Success',
                activities
            });
        }).catch((e) => {
            res.status(400).send({
                errorCode: 1,
                msg: 'Error!',
                errorDetails: e
            });
        });
}

exports.testActivity = (req, res) => {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    var mongooseLogs = require('mongoose-activitylogs');
    var BlogPost = new Schema({
        author: String,
        title: String,
        body: String,
        date: Date
    });

    var Post = mongoose.model('BlogPost', BlogPost);

    var user = new Schema({
        firstName: String,
        lastName: String,
        password: String,
        email: String
    });

    BlogPost.plugin(mongooseLogs, {
        schemaName: "blogpost",
        createAction: "posted",
        updateAction: "updated",
        deleteAction: "removed"
    });

    req.body = {
        author: '5d7b728bdd19a33ff4c2325f',
        title: "Post Title",
        body: "Post Body",
        date: new Date()
    }

    var payload = req.body;

    payload.modifiedBy = req.user; // or req.session.user;
    var post = new Post(payload);
    post.save()
        .then((post) => {
            res.send({
                errorCode: 0,
                msg: 'Success',
                post
            });
        }).catch((e) => {
            res.status(400).send({
                errorCode: 1,
                msg: 'Error!',
                errorDetails: e
            });
        });

    // payload.loggedBy = { 'name': "Test Modify by Tamilmathi Senthilkumar" };
    // var post = new userActivity(payload);
    // post.save();
}