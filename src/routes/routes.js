// const {
//     authenticate
// } = require('../middlewares/authenticate');
var Controller = require('../controllers/apiController');

// module.exports -so that we can import this file in other pages
module.exports = function (app) {
    app.get('/', function (req, res, next) {
        res.json({
            errorCode: 0,
            msg: 'Welcome to Alpaindia!!!!'
        });
    });

    app.route('/api/user/create')
        .post(Controller.userSave);

    app.route('/api/user/signin')
        .post(Controller.signIn);

    //catch 404
    app.use((req, res) => {
        var err = new Error('Not Found');
        err.errmsg = 'API Not Found';
        res.status(404).send({
            errorCode: 1,
            msg: 'Error!',
            errorDetails: err.errmsg
        });
    });
}