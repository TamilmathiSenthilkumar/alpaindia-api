var bodyParser = require('body-parser'); //to parse form data to json obj
var User = require('../models/user');
const { authenticate } = require('../middlewares/authenticate');
var Controller = require('../controllers/apiController');

// module.exports -so that we can import this file in other pages
module.exports = function (app) {
    app.get('/', function (req, res, next) {
        // res.json("Welcome to Pinterest!!!!");
        res.json({ errorCode: 0, msg: 'Welcome to DQSNxT API' });
    });


    //users
    app.route('/route/users')
        .get(authenticate, function (req, res, next) {
            User.find({})
                .sort({ 'createdAt': -1 })
                .then((user) => {
                    res.send({
                        errorCode: 0,
                        msg: 'Success',
                        user
                    });
                }).catch((err) => {
                    res.status(400).send({
                        errorCode: 1,
                        msg: 'Error!',
                        errorDetails: e
                    });
                });
        });

    app.route('/users')
        .get(authenticate, Controller.getUsers);

    app.route('/user/create')
        .post(function (req, res, next) {
            var user = new User();
            user.email = req.body.email;
            user.password = req.body.password;

            // var foundUser = User.findOne({ email: user.email });
            // if (foundUser.length > 0) {
            //     res.status(400).send({
            //         errorCode: 1,
            //         msg: 'Failed',
            //         errorDetails: 'Email Already In Use!!!'
            //     });
            // }

            user.save().then(() => {
                return user.generateAuthToken()
            }).then((token) => {
                res.send({
                    errorCode: 0,
                    msg: 'Success',
                    user
                });
            }).catch((err) => {
                res.status(400).send({
                    errorCode: 1,
                    msg: 'Error!',
                    errorDetails: e
                });
            });
        });

    app.route('/api/user/signin')
        .post((req, res, next) => {
            var body = req.body;

            User.findByCredentials(body.email, body.password)
                .then((user) => {
                    console.log(user.tokens[0].token);
                    res.header('X-Auth', user.tokens[0].token).send({
                        errorCode: 0,
                        msg: 'Success',
                        user
                    });
                }).catch((e) => {
                    res.status(400).send({
                        errorCode: 1,
                        msg: 'Error!',
                        errorDetails: e
                    });
                });

        });
}

