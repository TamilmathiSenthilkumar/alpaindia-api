const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
// const _ = require('lodash');
const bcrypt = require('bcryptjs');
// const crypto = require('crypto');

var Schema = mongoose.Schema;

var UserSchema = new Schema({

    firstName: {
        type: String,
        minlength: 3,
        required: true
    },
    lastName: {
        type: String,
        minlength: 1,
        required: true
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        trim: true,
        unique: true,
        index: true,
        sparse: true
    },
    password: {
        type: String,
    },
    // thumbnail: {
    //     type: String
    // },
    authType: {
        type: String,
        required: true,
        default: 'Custom'
    },
    tokens: [{
        access: {
            type: String,
            required: true,
        },
        token: {
            type: String,
            required: true,
        }
    }]
}, {
    timestamps: true
});


UserSchema.methods.generateAuthToken = function () {
    var user = this;
    var access = 'auth';
    var token = jwt.sign({
        _id: user._id.toHexString(),
        access
    }, process.env.JWT_SECRET).toString();
    user.tokens = user.tokens.concat([{
        access,
        token
    }]);
    return user.save().then(() => {
        return token;
    });

}

UserSchema.methods.removeToken = function (token) {

    var user = this;
    return user.update({
        $pull: {
            tokens: {
                token: token
            }
        }
    });
}

UserSchema.statics.findByToken = function (token) {

    var user = this;
    var decoded;

    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject('Invalid Token');
    }

    return user.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });

};

UserSchema.statics.findByCredentials = function (email, password) {

    var user = this;

    return user.findOne({
            email
        })
        .then((user) => {
            if (!user) {
                return Promise.reject("Email is wrong!");
            }

            return new Promise((resolve, reject) => {
                bcrypt.compare(password, user.password, (err, res) => {
                    if (res) {
                        resolve(user);
                    } else {
                        reject("Password is wrong!");
                    }
                });
            });
        });
};

UserSchema.pre('save', function (next) {
    var user = this;
    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }

});

UserSchema.pre('findOneAndUpdate', function (next) {

    var user = this;
    var password = user.getUpdate().$set.password;

    if (typeof password !== 'undefined') {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, (err, hash) => {
                user.findOneAndUpdate({}, {
                    password: hash
                });
                next();
            });
        });
    } else {
        next();
    }

});

var User = mongoose.model('User', UserSchema);
// module.exports = { User };
module.exports = mongoose.model('User', UserSchema);